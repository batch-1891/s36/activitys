const express = require('express')
const TaskController = require('../controllers/TaskController.js')
const router = express.Router()


router.post('/create', (request, response) => {
	TaskController.createTask(request.body).then((task) => response.send(task))
})

router.get('/:id/complete', (request, response) => {
	TaskController.findTask(request.params.id).then((task) => response.send(task))
})

router.put('/:id/complete', (request, response) => {
	// updateTask requires 2 arguments:
	/*
		1. ID of the task to be updated
		2. New content of the task
	*/
	TaskController.updateTask(request.params.id, request.body).then((updatedTask) => response.send(updatedTask))
})

module.exports = router 