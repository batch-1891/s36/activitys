const Task = require('../models/Task.js')

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	})
}

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((savedTask, error) => {
		if(error){
			return error
		}

		return savedTask
	})
}

module.exports.findTask = (taskId) => {
	return Task.findById(taskId).then((task, error) => {
		if(error) {
			return error
		}

		return task
	})
}

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if (error){
			return error
		}

		result.name = newContent.name

		return result.save().then((updatedTask, error) => {
			if (error) {
				return error
			}

			return updatedTask
		})
	})
}




